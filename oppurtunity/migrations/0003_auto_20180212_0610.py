# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-12 00:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oppurtunity', '0002_auto_20171109_1308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='opportunity',
            name='currency',
            field=models.CharField(blank=True, choices=[('MWK', 'MWK, Kwacha'), ('RWF', 'RWF, Franc'), ('USD', '$, Dollar'), ('ZAR', 'ZAR, Rand')], max_length=3, null=True),
        ),
        migrations.AlterField(
            model_name='opportunity',
            name='lead_source',
            field=models.CharField(blank=True, choices=[('NONE', 'NONE'), ('CALL', 'CALL'), ('EMAIL', ' EMAIL'), ('EXISTING CUSTOMER', 'EXISTING CUSTOMER'), ('PARTNER', 'PARTNER'), ('CAMPAIGN', 'CAMPAIGN'), ('WEBSITE', 'WEBSITE'), ('OTHER', 'OTHER')], max_length=255, null=True, verbose_name='Source of Lead'),
        ),
        migrations.AlterField(
            model_name='opportunity',
            name='stage',
            field=models.CharField(choices=[('QUALIFICATION', 'QUALIFICATION'), ('NEEDS ANALYSIS', 'NEEDS ANALYSIS'), ('VALUE PROPOSITION', 'VALUE PROPOSITION'), ('ID.DECISION MAKERS', 'ID.DECISION MAKERS'), ('PROPOSAL/PRICE QUOTE', 'PROPOSAL/PRICE QUOTE'), ('NEGOTIATION/REVIEW', 'NEGOTIATION/REVIEW'), ('CLOSED WON', 'CLOSED WON'), ('CLOSED LOST', 'CLOSED LOST')], max_length=64, verbose_name='Stage'),
        ),
    ]
