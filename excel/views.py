from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseBadRequest
from django import forms
from django.template import RequestContext
import django_excel as excel
from excel.models import Question, Choice
from quotations.models import OrderItem, Item
from django.shortcuts import *
from django.template.loader import get_template, render_to_string
from django.template import RequestContext
import pyexcel as pe
import sys
from weasyprint import HTML, CSS
import tempfile
# Create your views here.

class UploadFileForm(forms.Form):
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'class': 'inputfile'}))

def calcTotal(unit, quantity):
    return unit * quantity;

def import_excel(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)

        if form.is_valid():
            sub_total = 0
            parts = []
            data = request.FILES['file'].get_array()
            for i in data[1:]:
                total = calcTotal(i[1], i[2])
                i.append(total)
                parts.append(i)
                sub_total = sub_total + total
            grand_total = sub_total + (16.5*sub_total/100)
            context = {'grand_total': grand_total, 'parts': parts, 'data':data, 'sub_total':sub_total}
            html_string = render_to_string('excel/excel_quote.html', context)
            pdf_file = HTML(string=html_string)
            result = pdf_file.write_pdf(stylesheets=['/Users/user/Sparc-CRM/static/css/quotation.css'])

            response = HttpResponse(content_type='application/pdf;')
            response['Content-Disposition'] = 'inline; filename=quote.pdf'
            response['Content-Transfer-Encoding'] = 'binary'

            with tempfile.NamedTemporaryFile(delete=True) as output:
                output.write(result)
                output.flush()
                output = open(output.name, 'rb')
                response.write(output.read())

            return response
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        'excel/upload_form.html',
        {
            'form': form,
            'title': 'Import excel data into database example',
            'header': 'Please select an excel file to import:'
})

def excel_quote_pdf(request):
    html_string = render_to_string('quotations/quotation.html', {'quote': quote, 'total': total})
    pdf_file = HTML(string=html_string)
    result = pdf_file.write_pdf(stylesheets=['/Users/user/Sparc-CRM/static/css/quotation.css'])

    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=quote.pdf'
    response['Content-Transfer-Encoding'] = 'binary'

    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
    quote.clear()
    return response

def handson_table(request):
    return excel.make_response_from_tables([Question, Choice], 'handsontable.html')

def upload(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            filehandle = request.FILES['file']
            return redirect('handson_view')
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(request, 'excel/upload_form.html', {'form': form})

def download(request):
    sheet = excel.pe.Sheet([[1, 2],[3, 4]])
    return excel.make_response(sheet, "csv")
