from django.conf.urls import url
from excel import views

app_name = 'excel'


urlpatterns = [
    url(r'^upload/$', views.upload, name='upload'),
    url(r'^download/$', views.download, name='download'),
    url(r'^import_excel/$', views.import_excel, name='import_excel'),
    url(r'^handson_view/', views.handson_table, name='handson_view'),
]
