# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-14 00:59
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quotations', '0004_item_upload'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='upload',
        ),
    ]
