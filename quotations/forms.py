from django import forms
from django.forms import ModelForm, formset_factory
from quotations.models import OrderItem


class SaleQuoteForm(ModelForm):
    
    #part = forms.CharField(label="Part#", max_length=255)
    #description = forms.CharField(label="Description")
    #quantity = forms.IntegerField(label="Quantity")
    #unit_price = forms.DecimalField(decimal_places=2, max_digits=12)
    #total_price = (self.quantity * self.unit_price)

    class Meta:
        model = OrderItem
        fields = ['part', 'description', 'quantity', 'unit_price', 'markup', 'duty',
                  'shipment', 'installation']
        widgets = {
            'part': forms.TextInput(attrs={'placeholder': 'Part', 'class': 'form-control'}),
            'description': forms.TextInput(attrs={'placeholder': 'Description', 'class': 'form-control'}),
            'quantity': forms.TextInput(attrs={'placeholder': 'quantity', 'class': 'form-control'}),
            'unit_price': forms.TextInput(attrs={'placeholder': 'Unit Price', 'class': 'form-control'}),
            'markup': forms.TextInput(attrs={'placeholder': 'Mark Up', 'class': 'form-control'}),
            'duty': forms.TextInput(attrs={'placeholder': 'Duty', 'class': 'form-control'}),
            'shipment': forms.TextInput(attrs={'placeholder': 'Shipment', 'class': 'form-control'}),
            'installation': forms.TextInput(attrs={'placeholder': 'Installation', 'class': 'form-control'}),

        }
       

'''
    def __init__(self, *args, **kwargs):
        super(SaleQuoteForm, self).__init__(*args, **kwargs)
        self.fields['part'].widget.attrs['placeholder'] = 'Part #'
        self.fields['description'].widget.attrs['placeholder'] = 'Description'
        self.fields['quantity'].widget.attrs['placeholder'] = 'Quantity'
        self.fields['unit_price'].widget.attrs['placeholder'] = 'Unit Price'
        self.fields['total_price'] = 'Total Price'
'''

#SalesFormset = formset_factory(SaleQuoteForm)


class SupportQuoteForm(forms.Form):
    csi = forms.IntegerField(label="CSI #")
    serial = forms.CharField(label="Serial Number")
    description = forms.CharField(label="Description")
    start_date = forms.DateTimeField()
    end_date = forms.DateTimeField()
    quantity = forms.IntegerField()
    unit_price = forms.DecimalField(decimal_places=2, max_digits=12)
    markup = forms.DecimalField(decimal_places=9, max_digits=12)
    duty = forms.DecimalField(decimal_places=9, max_digits=12)
    shipment = forms.DecimalField(decimal_places=9, max_digits=12)
    installation = forms.DecimalField(decimal_places=9, max_digits=12)
    total_price = forms.DecimalField(decimal_places=2, max_digits=12)
