# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.forms import formset_factory
from django.conf import settings
from .forms import SaleQuoteForm
from django.shortcuts import *
from django.template.loader import get_template, render_to_string
from django.template import RequestContext
from django.forms.formsets import formset_factory
from weasyprint import HTML, CSS
import tempfile


from quotations.forms import *
# Create your views here.
def calcTotal(price0, markup0, duty0, shipment0, installation0):
    per_markup = (markup0/100)*price0
    per_duty = (duty0/100)*price0
    add_price = price0 + per_duty + per_markup + shipment0 + installation0
    return add_price

def sales_quote(request):
    sales_formset = formset_factory(SaleQuoteForm)
    quotation_number = []

    if request.method == 'POST':
        quote_session = request.session.get('quote_session', 'u')
        total_session = request.session.get('total_session', 'u')
        grand_total_session = request.session.get('grand_total_session', 'u')
        sale_formset = sales_formset(request.POST)

        if sale_formset.is_valid():
            count = sale_formset.total_form_count()
            message = "Valid form"
            parts = {}
            prices = []
            quote = []
            for sales_form, i in zip(sale_formset, range(count)):
                cd = sales_form.cleaned_data
                part = cd['part']
                description = cd['description']
                quantity = cd['quantity']
                unit_price = cd['unit_price']
                markup = cd['markup']
                duty = cd['duty']
                shipment = cd['shipment']
                installation = cd['installation']
                price = calcTotal(price0=unit_price,
                                        markup0=markup, duty0=duty, shipment0=shipment,
                                        installation0=installation)
                total_price = price * quantity
                prices.append(total_price)
                parts = {'fpart': part, 'fquantity': str(quantity), 'fdescription': str(description),
                              'funit_price': str(price), 'fmarkup': str(markup), 'fduty': str(duty),
                         'ftotal_price': str(total_price)}
                quote.append(parts)
            grand = sum(price for price in prices)
            grand_total = grand + (16*grand/100)
            request.session['total_session'] = str(grand)
            request.session['grand_total_session'] = str(grand_total)
            request.session['quote_session'] = quote

            return redirect('quotations:quote_pdf')
        else:
            return HttpResponse(sale_formset.errors)

        context = {'part': part, 'description': description, 'quantity': quantity,
                   'unit_price': unit_price, 'total_price': total_price, 'sale_formset': sale_formset,
                   'markup': markup, 'duty': duty, 'shipment': shipment, 'installation': installation}
        return render_to_response('quotations/quote_preview.html',
                                  {'message': message}, context)

    else:
        sale_formset = sales_formset()
    context = {'sale_formset': sale_formset}

    return render(request, 'quotations/quote.html', context)

def quote_pdf(request):
    if "quote_session" in request.session:
        quote = request.session['quote_session']
        total = request.session['total_session']
        grand_total = request.session['grand_total_session']
    else:
        pass
    html_string = render_to_string('quotations/quotation.html', {'quote': quote, 'total': total, 'grand_total':grand_total})
    pdf_file = HTML(string=html_string)
    result = pdf_file.write_pdf(stylesheets=['/Users/user/Sparc-CRM/static/css/quotation.css'])

    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=quote.pdf'
    response['Content-Transfer-Encoding'] = 'binary'

    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
    quote.clear()
    return response
