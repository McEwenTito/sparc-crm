from django.conf.urls import url
from quotations import views

app_name = 'quotations'


urlpatterns = [
    url(r'^sales_quote/$', views.sales_quote, name='sales_quote'),
    url(r'^quote_pdf/$', views.quote_pdf, name='quote_pdf'),


]
