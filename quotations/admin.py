# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from quotations.models import OrderItem, Item
# Register your models here.
admin.site.register(OrderItem)
admin.site.register(Item)
