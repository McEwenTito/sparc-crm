from django.db import models
import datetime
from uuid import uuid4

class OrderItem(models.Model):
    part = models.CharField(max_length=255)
    description = models.CharField(max_length=1024)
    quantity = models.IntegerField()
    unit_price = models.DecimalField(decimal_places=2, max_digits=12)
    markup = models.DecimalField(decimal_places=2, max_digits=12)
    duty = models.DecimalField(decimal_places=2, max_digits=12)
    shipment = models.DecimalField(decimal_places=2, max_digits=12)
    installation = models.DecimalField(decimal_places=2, max_digits=12)

class Item(models.Model):

    name = models.CharField(max_length=255)
    quantity = models.CharField(max_length=255)
    price = models.CharField(max_length=255)

class Quotation(object):
    """docstring for Quotation."""
    def __init__(self):
        super(Quotation, self).__init__()
        now = datetime.datetime.now()
        self.uid = str(now.year)+str(now.month)+str(now.day)
    uid = models.CharField(editable=False)
